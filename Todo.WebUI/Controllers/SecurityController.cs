﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Todo.Repositories;
using Todo.WebUI.Code.Managers;
using Todo.WebUI.Models;

namespace Todo.WebUI.Controllers
{
    public class SecurityController : Controller
    {
        private readonly ISecurityManager _manager;
        public SecurityController(ISecurityManager manager)
        {
            this._manager = manager;
        }
        //
        // GET: /Security/

        [HttpGet]
        public ActionResult Login()
        {
            if (_manager.IsAuthenticated())
            {
                return RedirectToAction("Index", "task");
            }
            ViewBag.Error = null;
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            
            if(_manager.Login(model.UserName, model.Password))
            {
                return RedirectToAction("Index", "task");
            }
            ViewBag.Error = "there is no such login or password uncorect";
            return View();
        }

        public ActionResult Logout()
        {
            if (_manager.IsAuthenticated())
            {
                _manager.Logout();
            }
            return RedirectToAction("Login", "Security");
        }
    }
}
