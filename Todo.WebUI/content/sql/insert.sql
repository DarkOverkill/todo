﻿USE TaskDataBase

GO

INSERT INTO Tasks (Id, [Description], IsDone, [Priority]) VALUES 
	('1', 'To do task number 1. For this must be done next...', '0', '3'),
	('2', 'To do task number 2. For this must be done next...', '0', '2'),
	('3', 'To do task number 3. For this must be done next...', '1', '4'),
	('4', 'To do task number 4. For this must be done next...', '0', '1');

GO

INSERT INTO Users (UserName, [Password], Email) VALUES
	('admin', '1234', 'admin@gmail.com'),
	('user', '4321', 'user@gmail.com')

UPDATE [dbo].[Users] SET [Password] ='7E2FEAC95DCD7D1DF803345E197369AF4B156E4E7A95FCB2955BDBBB3A11AFD8BB9D35931BF15511370B18143E38B01B903F55C5ECBDED4AF99934602FCDF38C' WHERE UserName = 'user'
select * from Users