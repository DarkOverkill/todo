﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Todo.WebUI.Models
{
    public class TaskModel
    {
        public int Id { get; set; }
        public string TaskInfo { get; set; }
        public bool Done { get; set; }
    }
}