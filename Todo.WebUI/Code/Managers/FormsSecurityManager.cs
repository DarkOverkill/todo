﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using Todo.Repositories;
using Todo.WebUI.Code.Service;

namespace Todo.WebUI.Code.Managers
{
    public class FormsSecurityManager : ISecurityManager
    {
        private readonly IUserRepository userRepository;     
        public FormsSecurityManager(IUserRepository repository)
        {
            this.userRepository = repository;
        }
        public bool Login(string user, string password)
        {
            string hashPassword = new Sha512HashService().Hash(password);
            if (userRepository.Login(user, hashPassword))
           {
               FormsAuthentication.SetAuthCookie(user, false);
               return true;
           }
            return false;
        }
        public bool IsAuthenticated()
        {
            var user = HttpContext.Current.User;
            return (user != null) && (user.Identity != null) && (user.Identity.IsAuthenticated == true);
        }
        public void Logout()
        {
            FormsAuthentication.SignOut();
        }
    }
}