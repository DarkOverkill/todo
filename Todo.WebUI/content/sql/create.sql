CREATE DATABASE TaskDataBase

GO

USE TaskDataBase

GO

CREATE TABLE Tasks(
	Id int not null identity(1,1),
	[Description] nvarchar(128) not null,
	IsDone int,
	[Priority] int
);

GO 

CREATE TABLE Users(
	Id int not null identity(1,1),
	UserName nvarchar(64) not null,
	[Password] nvarchar(512) not null,
	Email nvarchar(64) not null
	
	constraint pk_users_id primary key(Id)
);
