﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Todo.Repositories
{
    public class SQLUserRepository : IUserRepository
    {
        private static string connectionString = ConfigurationManager.ConnectionStrings["TaskDataBase"].ConnectionString;

        public bool Login(string userName, string password)
        {           
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = "SELECT [UserName], [Password] FROM [dbo].[Users] WHERE [UserName] = @userName";
                    command.Parameters.AddWithValue("@userName", userName);
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            reader.Read();
                            if (password == (string)reader["Password"])
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }
    }
}
