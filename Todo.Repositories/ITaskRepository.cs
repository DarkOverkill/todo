﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Entities;

namespace Todo.Repositories
{
    public interface ITaskRepository
    {
        List<TaskEntity> GetAll();
        List<TaskEntity> GetAllSortByPriority();
        void Add(TaskEntity newTask);
        void Delete(int id);
        void EditTask(TaskEntity editTask);
        TaskEntity GetById(int id);
        List<TaskEntity> SortingByElement(string sortVal, string elemName);
    }
}
