﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Entities;

namespace Todo.Repositories
{
    public class SQLTaskRepository : ITaskRepository
    {
        private static List<TaskEntity> sqlData = new List<TaskEntity>();
        //string connectionString = ConfigurationManager.ConnectionStrings["TaskDataBase"].ConnectionString;
        string connectionString;
        public SQLTaskRepository(string _connString)
        {
            this.connectionString = _connString;
        }

        public void Add(TaskEntity newTask)
        {
            newTask.Id = sqlData.Max(item => item.Id + 1);
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = "INSERT INTO [dbo].[Tasks] ([Id], [Description], [IsDone], [Priority]) VALUES (@Id, @Description, @IsDone, @Priority);";

                    command.Parameters.AddWithValue("@Id", newTask.Id);
                    command.Parameters.AddWithValue("@Description", newTask.TaskInfo);
                    command.Parameters.AddWithValue("@IsDone", newTask.Done);
                    command.Parameters.AddWithValue("@Priority", newTask.Priority);

                    command.ExecuteNonQuery();
                }
            }
        }

        public void Delete(int id)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = "DELETE FROM [dbo].[Tasks] WHERE [Id] = @Id";

                    command.Parameters.AddWithValue("@Id", id);

                    command.ExecuteNonQuery();
                }
            }
        }

        public void EditTask(TaskEntity editTask)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = "UPDATE [dbo].[Tasks] SET [Description] = @Description, [IsDone] = @IsDone, [Priority] = @Priority WHERE [Id] = @Id";

                    command.Parameters.AddWithValue("@Description", editTask.TaskInfo);
                    command.Parameters.AddWithValue("@IsDone", editTask.Done);
                    command.Parameters.AddWithValue("@Id", editTask.Id);
                    command.Parameters.AddWithValue("@Priority", editTask.Priority);

                    command.ExecuteNonQuery();
                }
            }
        }

        public List<TaskEntity> GetAll()
        {
            sqlData = new List<TaskEntity>();
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = "SELECT [Id], [Description], [IsDone], [Priority] FROM [dbo].[Tasks] ORDER BY [Id]";
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            TaskEntity sqlTask = new TaskEntity();
                            sqlTask.Id = (int)reader["Id"];
                            sqlTask.TaskInfo = (string)reader["Description"];
                            sqlTask.Done = (int)reader["IsDone"] == 0 ? false : true;
                            sqlTask.Priority = (int)reader["Priority"];
                            sqlData.Add(sqlTask);
                        }
                    }
                }
            }
            return sqlData;
        }

        public List<TaskEntity> GetAllSortByPriority()
        {
            sqlData = new List<TaskEntity>();
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = "SELECT [Id], [Description], [IsDone], [Priority] FROM [dbo].[Tasks] ORDER BY [Id]";
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            TaskEntity sqlTask = new TaskEntity();
                            sqlTask.Id = (int)reader["Id"];
                            sqlTask.TaskInfo = (string)reader["Description"];
                            sqlTask.Done = (int)reader["IsDone"] == 0 ? false : true;
                            sqlTask.Priority = (int)reader["Priority"];
                            sqlData.Add(sqlTask);
                        }
                    }
                }
            }
            sqlData.Sort(delegate (TaskEntity x, TaskEntity y)
            {
                if (x.Priority == y.Priority) return 0;
                else if (x.Priority > y.Priority) return 1;
                return -1;
            });
            return sqlData;
        }
        public TaskEntity GetById(int Id)
        {
            TaskEntity task = new TaskEntity();
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = "SELECT [Id], [Description], [IsDone], [Priority] FROM [dbo].[Tasks] WHERE [Id] = @TaskId";

                    command.Parameters.AddWithValue("@TaskId", Id);

                    using (var reader = command.ExecuteReader())
                    {
                        reader.Read();
                        task.Id = (int)reader["Id"];
                        task.TaskInfo = (string)reader["Description"];
                        task.Done = (int)reader["IsDone"] == 0 ? false : true;
                        task.Priority = (int)reader["Priority"];
                    }
                }
            }
            return task;
        }
        public List<TaskEntity> SortingByElement(string sortVal, string elemName)
        {
            sqlData = new List<TaskEntity>();
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand())
                {
                    string selectCommand = "SELECT[Id], [Description], [IsDone], [Priority] FROM[dbo].[Tasks] ORDER BY[" + elemName + "]";
                    command.Connection = connection;
                    command.CommandType = System.Data.CommandType.Text;
                    if (sortVal == "up")
                    {
                        command.CommandText = selectCommand;
                    }
                    else
                    {
                        
                        command.CommandText = selectCommand + " DESC";
                    }
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            TaskEntity sqlTask = new TaskEntity();
                            sqlTask.Id = (int)reader["Id"];
                            sqlTask.TaskInfo = (string)reader["Description"];
                            sqlTask.Done = (int)reader["IsDone"] == 0 ? false : true;
                            sqlTask.Priority = (int)reader["Priority"];
                            sqlData.Add(sqlTask);
                        }
                    }
                }
            }
            return sqlData;
        }
    }
}
