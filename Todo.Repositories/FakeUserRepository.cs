﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Entities;

namespace Todo.Repositories
{
    public class FakeUserRepository : IUserRepository
    {
        private readonly string userName;
        private readonly string password;
        public FakeUserRepository()
        {
            this.userName = "admin";
            this.password = "1234";
        }
        public bool Login(string _userName, string _password)
        {
            if(_userName == userName && _password == password)
            {
                return true;
            }
            return false;
        }
        public void Logout()
        {

        }
    }
}
