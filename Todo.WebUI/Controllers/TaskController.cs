﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Todo.Repositories;
using Todo.Entities;
using Todo.WebUI.Models;

namespace Todo.WebUI.Controllers
{
    [Authorize]
    public class TaskController : Controller
    {
        private readonly ITaskRepository taskRepository;
        public TaskController(ITaskRepository repositroy)
        {
            this.taskRepository = repositroy;
            //string _connString = @"Server=INTERCEPTOR\SQLEXPRESS;Database=TaskDataBase;Integrated Security=SSPI";
            //string _connString = @"Data Source=10.7.1.10;database=TaskDataBase;User Id=sa;Password=123456";
            //taskRepository = new SQLTaskRepository(_connString);
            //taskRepository = new FakeTaskRepository();
        }
        // GET: /Task/
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.Result = taskRepository.GetAll();
            ViewBag.Error = null;
            return View();
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            ViewBag.Result = taskRepository.GetById(id);
            return View();
        }
        [HttpPost]
        public ActionResult SaveEdit(int id, string taskInfo, bool done, int priority)
        {
            TaskEntity editTask = new TaskEntity() { Id = id, TaskInfo = taskInfo, Done = done, Priority = priority };
            taskRepository.EditTask(editTask);
            return Redirect("Index");
        }
        [HttpPost]
        public ActionResult Index(string newTask, int priority)
        {
            ViewBag.Error = "must be not empty or less 128 chars";
            if (newTask.Length <= 128 && !String.IsNullOrWhiteSpace(newTask))
            {
                TaskEntity task = new TaskEntity()
                {
                    TaskInfo = newTask,
                    Done = false,
                    Priority = priority
                };
                ViewBag.Error = null;
                taskRepository.Add(task);
            }
            ViewBag.Result = taskRepository.GetAll();
            return View();
        }
        [HttpPost]
        public ActionResult Delete(int taskId)
        {
            taskRepository.Delete(taskId);
            return Redirect("Index");
        }
        [HttpPost]
        public ActionResult SetStatus(int taskId, bool IsDone)
        {
            TaskEntity task = taskRepository.GetById(taskId);
            task.Done = IsDone;
            taskRepository.EditTask(task);
            return Json(new object());
        }
        [HttpGet]
        public ActionResult Filter(string status, int priority)
        {
            if (Convert.ToBoolean(priority))
            {
                if (status == "Done")
                {
                    return PartialView("TasksPartial", taskRepository.GetAll().FindAll(isDone => isDone.Done == true).
                        FindAll(_priority => _priority.Priority == priority));
                }
                else if (status == "Inprogress")
                {
                    return PartialView("TasksPartial", taskRepository.GetAll().FindAll(isDone => isDone.Done == false).
                        FindAll(_priority => _priority.Priority == priority));
                }
                else
                {
                    return PartialView("TasksPartial", taskRepository.GetAll().FindAll(_priority => _priority.Priority == priority));
                }
            }
            else
            {
                if (status == "Done")
                {
                    return PartialView("TasksPartial", taskRepository.GetAll().FindAll(isDone => isDone.Done == true));
                }
                else if (status == "Inprogress")
                {
                    return PartialView("TasksPartial", taskRepository.GetAll().FindAll(isDone => isDone.Done == false));
                }
            }
            return PartialView("TasksPartial", taskRepository.GetAll());
        }
        [HttpGet]
        public ActionResult Sorting(string sortVal, string elemName)
        {
            return PartialView("TasksPartial", taskRepository.SortingByElement(sortVal, elemName));
        }
    }
}