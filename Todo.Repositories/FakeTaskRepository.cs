﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Entities;

namespace Todo.Repositories
{
    public class FakeTaskRepository : ITaskRepository
    {
        private static List<TaskEntity> fakeList;
        static FakeTaskRepository()
        {
                fakeList = new List<TaskEntity>();
                fakeList.Add(new TaskEntity()
                {
                    Id = 1,
                    TaskInfo = "Do task 1",
                    Done = false,
                    Priority = 2,
                });
                fakeList.Add(new TaskEntity()
                {
                    Id = 2,
                    TaskInfo = "Do task 2",
                    Done = true,
                    Priority = 3,
                });
                fakeList.Add(new TaskEntity()
                {
                    Id = 4,
                    TaskInfo = "Do task 4",
                    Done = false,
                    Priority = 1,                   
                });
            fakeList.Add(new TaskEntity()
            {
                Id = 3,
                TaskInfo = "Do task 3",
                Done = false,
                Priority = 4,
            });
        }       
        public List<TaskEntity> GetAll()
        {
            fakeList.Sort(delegate (TaskEntity x, TaskEntity y)
            {
                if (x.Id == y.Id) return 0;
                else if (x.Id > y.Id) return 1;
                return -1;
            });
            return fakeList;
        }
        public List<TaskEntity> GetAllSortByPriority()
        {
            fakeList.Sort(delegate (TaskEntity x, TaskEntity y)
            {
                if (x.Priority == y.Priority) return 0;
                else if (x.Priority > y.Priority) return 1;
                return -1;
            });
            return fakeList;
        }
        public void Add(TaskEntity newTask)
        {
            newTask.Id = fakeList.Max(item => item.Id + 1);
            fakeList.Add(newTask);
        }
        public void Delete(int id)
        {
            fakeList.RemoveAll(item => (item.Id == id));
        }
        public void EditTask(TaskEntity editTask)
        {
            fakeList.Find(item => (item.Id == editTask.Id)).TaskInfo = editTask.TaskInfo;
            fakeList.Find(item => (item.Id == editTask.Id)).Done = editTask.Done;
            fakeList.Find(item => (item.Id == editTask.Id)).Priority = editTask.Priority;
        }
        public TaskEntity GetById(int id)
        {
            return fakeList.Find(item =>(item.Id == id));
        }
        public List<TaskEntity> SortingByElement(string sortVal, string elemName)
        {
            return fakeList;
        }
    }
}
