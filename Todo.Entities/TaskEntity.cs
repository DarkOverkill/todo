﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todo.Entities
{
    public class TaskEntity
    {
        public int Id { get; set; }
        public string TaskInfo { get; set; }
        public bool Done { get; set; }
        public int Priority { get; set; }     
    }
}
