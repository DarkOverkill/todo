﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Todo.Repositories;
using Todo.Entities;
using Todo.WebUI.Models;

namespace Todo.WebUI.Controllers
{
    public class TaskController : Controller
    {
        //
        // GET: /Task/
        [HttpGet]
        public ActionResult Index()
        {
            ITaskRepository fakeData = new FakeTaskRepository();
            ViewBag.Result = fakeData.GetAll();
            ViewBag.Error = null;
            return View();
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            ITaskRepository fakeData = new FakeTaskRepository();
            ViewBag.Result = fakeData.GetAll().Find(item =>(item.Id == id));
          
            return View();
        }
        [HttpPost]
        public ActionResult Index(string newTask)
        {
            ViewBag.Error = "must be not empty";
            ITaskRepository fakeData = new FakeTaskRepository();
            if (newTask.Length <= 128 && !String.IsNullOrWhiteSpace(newTask))
            {
                TaskEntity task = new TaskEntity()
                {
                    TaskInfo = newTask,
                    Done = false
                };
                ViewBag.Error = null;
                fakeData.Add(task);
            }          
           ViewBag.Result = fakeData.GetAll();
           return View();
        }
        [HttpPost]
        public ActionResult Delete(int taskID)
        {
            ITaskRepository fakeData = new FakeTaskRepository();
            fakeData.Delete(taskID);
            return Redirect("Index");
        }
    }
}
