﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todo.Entities
{
    public static class EnumPriority
    {
        public enum name
        {
            Low = 1, Medium = 2, High = 3, Critical = 4
        };
        public enum color
        {
            green = 1, yellow = 2, orange = 3, red = 4
        };
    }
}
