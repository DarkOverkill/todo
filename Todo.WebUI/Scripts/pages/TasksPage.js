﻿var onCheckBoxClick = function (e) {
    console.log("arguments", arguments);
    var $el = $(e.target);
    var taskId = $el.data("task-id");
    var status = false;
    if ($el.is(":checked")) {
        status = true;
    } else {
        status = false;
    }
    console.log("task ID: " + taskId);
    var data = {
        taskId: taskId,
        IsDone: status
    };

    $.ajax({
        type: "POST",
        url: "/task/SetStatus",
        dataType: "json",
        data: data
    }).done(function () {
        if (data.IsDone == true) {
            $el.parent().parent().addClass("done");
        } else {
            $el.parent().parent().removeClass("done");
        }
        console.log("ajax-done");
    }).fail(function () {
        console.log("ajax-fail");
    })
}

var onListBoxChange = function (e) {
    console.log(e.target.value);
    var data = {
        status: $("#listBoxStatus")[0].value,
        priority: $("#listBoxPriority")[0].value
    };
    console.log(data);
    $.ajax({
        type: "GET",
        url: "/task/Filter",
        dataType: "html",
        data: data
    }).done(function (data) {
        console.log("ajax-done");
        $("tbody").html(data);
        $(".checkBoxIsDone").on("click", onCheckBoxClick);
    }).fail(function () {
        console.log("Something wrong(");
    })
}

var sortingByElement = function (elem) {
    //console.log($(elem));
    //console.log($(elem)[0].target.id);
    //console.log("done click");
    console.log($(elem.target).attr("data-value"));
    console.log($(elem.target).attr("id"));
    //console.log("element ID = " + $('#DoneHead').attr('id'));
    for (var i = 0; i < $(elem.target).parent().children().length; ++i)
    {
        //console.log($(elem.target).parent().children(i));
        $(elem.target).parent().children(i).children().remove();
    }
    if ($(elem.target).attr("data-value") == "up") {
        $(elem.target).attr("data-value", "down");
        $(elem.target).children().remove();
        $(elem.target).append('<span>&#8593;</span>');
    }
    else {
        $(elem.target).attr("data-value", "up");
        $(elem.target).children().remove();
        $(elem.target).append('<span>&#8595;</span>');
    }
    console.log($(elem.target).attr("data-value"));
    var data = {
        sortVal: $(elem.target).attr("data-value"),
        elemName :$(elem.target).attr("id")
    }
    $.ajax({
        type: "GET",
        url: "/task/Sorting",
        datatype: "html",
        data: data
    }).done(function (data) {
        console.log("ajax done");
        $("tbody").html(data);
        $(".checkBoxIsDone").on("click", onCheckBoxClick);
    }).fail(function (data) {
        console.log("ajax fail")
    });
};

var init = function () {
    $(".checkBoxIsDone").on("click", onCheckBoxClick);
    $("#listBoxStatus, #listBoxPriority").on("change", onListBoxChange);
    $(".headStyle").on("click", sortingByElement);
}

$(function () {
    init();
})