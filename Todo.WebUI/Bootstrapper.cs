using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc4;
using Todo.Repositories;
using Todo.WebUI.Code.Managers;

namespace Todo.WebUI
{
  public static class Bootstrapper
  {
    public static IUnityContainer Initialise()
    {
      var container = BuildUnityContainer();

      DependencyResolver.SetResolver(new UnityDependencyResolver(container));

      return container;
    }

    private static IUnityContainer BuildUnityContainer()
    {
      var container = new UnityContainer();

      // register all your components with the container here
      // it is NOT necessary to register your controllers

      // e.g. container.RegisterType<ITestService, TestService>();    
      RegisterTypes(container);

      return container;
    }

    public static void RegisterTypes(IUnityContainer container)
    {
            //string _connString = @"Data Source=10.7.1.10;database=TaskDataBase;User Id=sa;Password=123456";
            string _connString = @"Server=INTERCEPTOR\SQLEXPRESS;Database=TaskDataBase;Integrated Security=SSPI";
            container.RegisterType<ITaskRepository, SQLTaskRepository>(new InjectionConstructor(_connString));
            //container.RegisterType<ITaskRepository, FakeTaskRepository>(new InjectionConstructor());
            container.RegisterType<IUserRepository, SQLUserRepository>();
            container.RegisterType<ISecurityManager, FormsSecurityManager>();
    }

  }
}